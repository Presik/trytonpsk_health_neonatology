
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


class Newborn(metaclass=PoolMeta):
    __name__ = 'health.newborn'
    STATES = {'readonly': Eval('state') == 'signed'}
