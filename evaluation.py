
from trytond.model import fields, ModelSQL, ModelView
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Or


class PatientEvaluation(ModelSQL, ModelView):
    __name__ = 'health.patient.evaluation'

    @classmethod
    def _get_specialty_record(cls):
        return super(PatientEvaluation, cls)._get_specialty_record() + ['health.evaluation.neonatology']


class EvaluationNeonatology(ModelSQL, ModelView):
    "Evaluation Neonatology"
    __name__ = 'health.evaluation.neonatology'
    STATES = {
        'readonly': Or(Eval('state') == 'done', Eval('state') == 'finished')
    }
    newborn = fields.Many2One('health.newborn', 'Newborn')
    notes = fields.Text('Notes')
