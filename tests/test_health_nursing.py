import unittest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase


class HealthneonatologyTestCase(ModuleTestCase):
    '''
    Test Health neonatology module.
    '''
    module = 'health_neonatology'


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        HealthneonatologyTestCase))
    return suite
